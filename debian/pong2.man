.\"                                      Hey, EMACS: -*- nroff -*-
.\" First parameter, NAME, should be all caps
.\" Second parameter, SECTION, should be 1-8, maybe w/ subsection
.\" other parameters are allowed: see man(7), man(1)
.TH PONG2 6 "Februar  8, 2005"
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
pong2 \- OpenGL version the arcade classic Pong
.SH SYNOPSIS
.B pong2
.RI [ options ]
.br
.SH DESCRIPTION
This manual page documents briefly the command line parameters for the game
.B pong2
.PP
.\" TeX users may be more comfortable with the \fB<whatever>\fP and
.\" \fI<whatever>\fP escape sequences to invode bold face and italics,
.\" respectively.
\fBpong2\fP is an up till now two player (networked) game inspired by
the classical "Pong" from Amiga, which adds another dimension to the
playing field. Crazy graphics, fast gameplay, great fun ;)
.SH OPTIONS
.TP
.B \-n set your name (default: Hans)
.TP
.B \-c connect to already running server (default: act as server)
.TP
.B \-p set alternative udp networking port (default: 6642)
.TP
.B \-w set x resolution in pixels (default: 1024)
.TP
.B \-h set y resolution in pixels (default: 768)
.TP
.B \-b set individual bitsperpixel (default: 32)
.TP
.B \-f operate in fullscreen mode (default: windowed, toggle with 'f' key)

See the README for more information.
.SH AUTHOR
pong2 was written by Johannes Jordan <fopref@donnergurgler.net>
.PP
This manual page was written by Reinhard Tartler <siretart@tauware.de>
